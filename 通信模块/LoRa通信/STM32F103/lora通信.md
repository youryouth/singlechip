

# LORA通信

首先不管是使用Lora，WIFI，NB-IOT模块，精英版或者正点原子的其他板子，串口应该都是固定的。所以可以使用相同的串口3初始化代码。

```c
void usart3_init(u32 bound)
{  

	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE); // GPIOB时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE); //串口3时钟使能

 	USART_DeInit(USART3);                           //复位串口3
   //USART3_TX   PB10
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;      //PB10
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
    GPIO_Init(GPIOB, &GPIO_InitStructure);          //初始化PB10
   
    //USART3_RX	  PB11
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;   //上拉输入
    GPIO_Init(GPIOB, &GPIO_InitStructure);          //初始化PB11
	
	USART_InitStructure.USART_BaudRate = bound;                     //波特率一般设置为9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;     //字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;          //一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;             //无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
	
	USART_Init(USART3, &USART_InitStructure); //初始化串口3
 
	USART_Cmd(USART3, ENABLE);                  //使能串口 
	
	//使能接收中断
    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);//开启中断   
	
	//设置中断优先级
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2 ;//抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
	
	TIM7_Int_Init(99,7199);	//10ms中断
	USART3_RX_STA=0;		//清零
	TIM_Cmd(TIM7,DISABLE);	//关闭定时器7
}
```

可以参考使用https://blog.csdn.net/qq_40318498/article/details/106234709，一般都需要对模块进行初始化。

往串口3发送AT指令，如果模块返回OK，则表明模块可用。下面这个函数是封装了串口发送代码

```c
u8 lora_send_cmd(u8 *cmd,u8 *ack,u16 waittime)
{
	u8 res=0; 
	USART3_RX_STA=0;
	if((u32)cmd<=0XFF)
	{
		while((USART3->SR&0X40)==0);//等待上一次数据发送完成  
		USART3->DR=(u32)cmd;
	}else u3_printf("%s\r\n",cmd);//发送命令
	
	if(ack&&waittime)		//需要等待应答
	{
	   while(--waittime)	//等待倒计时
	   { 
		  delay_ms(10);
		  if(USART3_RX_STA&0X8000)//接收到期待的应答结果
		  {
			  if(lora_check_cmd(ack))break;//得到有效数据 
			  USART3_RX_STA=0;
		  } 
	   }
	   if(waittime==0)res=1; 
	}
	return res;
} 
```

**LoRa_Init()**函数完成lora的初始化，包括模块的检测，引脚的初始化等等。

**void LoRa_Set(void)/**/Lora模块参数配置

```c
void LoRa_Set(void)
{
	u8 sendbuf[20];
	u8 lora_addrh,lora_addrl=0;
	
	usart3_set(LORA_TTLBPS_115200,LORA_TTLPAR_8N1);//进入配置模式前设置通信波特率和校验位(115200 8位数据 1位停止 无数据校验）
	usart3_rx(1);//开启串口3接收
	
	while(LORA_AUX);//等待模块空闲
	LORA_MD0=1; //进入配置模式
	delay_ms(40);
	Lora_mode=0;//标记"配置模式"
	
	lora_addrh =  (LoRa_CFG.addr>>8)&0xff;
	lora_addrl = LoRa_CFG.addr&0xff;
	sprintf((char*)sendbuf,"AT+ADDR=%02x,%02x",lora_addrh,lora_addrl);//设置设备地址
	lora_send_cmd(sendbuf,"OK",50);
	sprintf((char*)sendbuf,"AT+WLRATE=%d,%d",LoRa_CFG.chn,LoRa_CFG.wlrate);//设置信道和空中速率
	lora_send_cmd(sendbuf,"OK",50);
	sprintf((char*)sendbuf,"AT+TPOWER=%d",LoRa_CFG.power);//设置发射功率
	lora_send_cmd(sendbuf,"OK",50);
	sprintf((char*)sendbuf,"AT+CWMODE=%d",LoRa_CFG.mode);//设置工作模式
	lora_send_cmd(sendbuf,"OK",50);
	sprintf((char*)sendbuf,"AT+TMODE=%d",LoRa_CFG.mode_sta);//设置发送状态
	lora_send_cmd(sendbuf,"OK",50);
	sprintf((char*)sendbuf,"AT+WLTIME=%d",LoRa_CFG.wltime);//设置睡眠时间
	lora_send_cmd(sendbuf,"OK",50);
	sprintf((char*)sendbuf,"AT+UART=%d,%d",LoRa_CFG.bps,LoRa_CFG.parity);//设置串口波特率、数据校验位
	lora_send_cmd(sendbuf,"OK",50);

	LORA_MD0=0;//退出配置,进入通信
	delay_ms(40);
	while(LORA_AUX);//判断是否空闲(模块会重新配置参数)
	USART3_RX_STA=0;
	Lora_mode=1;//标记"接收模式"
	usart3_set(LoRa_CFG.bps,LoRa_CFG.parity);//返回通信,更新通信串口配置(波特率、数据校验位)
	Aux_Int(1);//设置LORA_AUX上升沿中断	
	
}
```

如果需要修改上述参数，则在进入该函数前修改即可。

数据发送

```c
//Lora模块发送数据
void LoRa_SendData(void)
{      
	static u8 num=0;
    u16 addr;
	u8 chn;
	u16 i=0; 
		
	if(LoRa_CFG.mode_sta == LORA_STA_Tran)//广播透明传输
	{
		sprintf((char*)Tran_Data,"ATK-LORA-01 TEST %d",num);
		u3_printf("%s\r\n",Tran_Data);
		LCD_Fill(0,195,240,220,WHITE); //清除显示
		Show_Str_Mid(10,195,Tran_Data,16,240);//显示发送的数据	
		
		num++;
		if(num==255) num=0;
		
	}else if(LoRa_CFG.mode_sta == LORA_STA_Dire)//定向传输
	{
		
		addr = (u16)obj_addr;//目标地址
		chn = obj_chn;//目标信道
		
		date[i++] =(addr>>8)&0xff;//高位地址
		date[i++] = addr&0xff;//低位地址
		date[i] = chn;//无线信道
		
		for(i=0;i<Dire_DateLen;i++)//数据写到发送BUFF
		{
			date[3+i] = Dire_Date[i];
		}	
		for(i=0;i<(Dire_DateLen+3);i++)
		{
			//往串口发送数据
			while(USART_GetFlagStatus(USART3,USART_FLAG_TC)==RESET);//循环发送,直到发送完毕   
			USART_SendData(USART3,date[i]); 
		}	
		
        //将十六进制的数据转化为字符串打印在lcd_buff数组
		sprintf((char*)wlcd_buff,"%x %x %x %x %x %x %x %x",
				date[0],date[1],date[2],date[3],date[4],date[5],date[6],date[7]);
		
		LCD_Fill(0,200,240,230,WHITE);//清除显示
		Show_Str_Mid(10,200,wlcd_buff,16,240);//显示发送的数据	
		
	    Dire_Date[4]++;//Dire_Date[4]数据更新
		
	}
			
}
```

数据接收，跟正常接收数据应该一致

```c
//Lora模块接收数据
void LoRa_ReceData(void)
{
    u16 i=0;
    u16 len=0;
   
	//有数据来了
	if(USART3_RX_STA&0x8000)
	{
		len = USART3_RX_STA&0X7FFF;
		USART3_RX_BUF[len]=0;//添加结束符
		USART3_RX_STA=0;

		for(i=0;i<len;i++)
		{
			while(USART_GetFlagStatus(USART1,USART_FLAG_TC)==RESET); //循环发送,直到发送完毕   
			USART_SendData(USART1,USART3_RX_BUF[i]); 
		}
		LCD_Fill(10,260,240,320,WHITE);
		if(LoRa_CFG.mode_sta==LORA_STA_Tran)//透明传输
		{	
			Show_Str_Mid(10,270,USART3_RX_BUF,16,240);//显示接收到的数据

		}else if(LoRa_CFG.mode_sta==LORA_STA_Dire)//定向传输
		{
			//将十六进制的数据转化为字符串打印在lcd_buff数组
			sprintf((char*)rlcd_buff,"%x %x %x %x %x",
			USART3_RX_BUF[0],USART3_RX_BUF[1],USART3_RX_BUF[2],USART3_RX_BUF[3],USART3_RX_BUF[4]);
				
			Show_Str_Mid(10,270,rlcd_buff,16,240);//显示接收到的数据	
		}
		memset((char*)USART3_RX_BUF,0x00,len);//串口接收缓冲区清0
	}
}

```

使用流程：

**LoRa_Init()**  - >  **LoRa_Set()** 



# AT指令

类似于初始化参数

### 设置设备地址

AT+ADDR=0xff,0x00[高地址和低地址]

### 设置信道和空中速率

AT+WLRATE=23,5，空中速率设置表

> #define  LORA_RATE_0K3  0 //0.3
> #define  LORA_RATE_1K2  1 //1.2
> #define  LORA_RATE_2K4  2 //2.4
> #define  LORA_RATE_4K8  3 //4.8
> #define  LORA_RATE_9K6  4 //9.6
> #define  LORA_RATE_19K2 5 //19.2

### 设置发射功率

AT+TPOWER=3

> #define LORA_PW_11dBm  0   //11dBm
> #define LORA_PW_14Bbm  1   //14dBm
> #define LORA_PW_17Bbm  2   //17dBm
> #define LORA_PW_20Bbm  3   //20dBm

### 设置工作模式

AT+CWMODE=0

#define LORA_MODE_GEN   0   //一般模式
#define LORA_MODE_WK    1   //唤醒模式
#define LORA_MODE_SLEEP 2   //省电模式

### 设置发送状态

AT+TMODE=0

> //发送状态
> #define LORA_STA_Tran 0 //透明传输
> #define LORA_STA_Dire 1 //定向传输



## 设置波特率

所以同理也需要配置串口3的波特率为115200（即相同）。

AT+UART=3,0 [波特率9600，8位数据无校验]

## 设置睡眠状态

AT+WLTIME=0 [1秒]

详细的文件在LORA->lora_cfg.h中有体现。

这里需要说明的是：我们配置的波特率是lora模块的，这里正点原子连接lora模块是usart3，

## 出厂参数设置

//设备出厂默认参数
#define LORA_ADDR    0                //设备地址
#define LORA_CHN     23               //通信信道
#define LORA_POWER   LORA_PW_20Bbm    //发射功率
#define LORA_RATE    LORA_RATE_19K2   //空中速率
#define LORA_WLTIME  LORA_WLTIME_1S   //休眠时间
#define LORA_MODE    LORA_MODE_GEN    //工作模式
#define LORA_STA     LORA_STA_Tran    //发送状态
#define LORA_TTLBPS  LORA_TTLBPS_9600 //波特率
#define LORA_TTLPAR  LORA_TTLPAR_8N1  //校验位  

```c
AT+ADDR=0x00,0x00
AT+WLRATE=23,5
AT+TPOWER=3
AT+CWMODE=0
AT+TMODE=0
AT+WLTIME=0
AT+UART=3,0
```



## 数据发送

 直接发送即可。也就是说我们使用的时候，只需要调用

> LoRa_Init();	
> LoRa_Set();

这两个函数即可。首先配置串口1（从上位机接收数据）和配置串口3（发送给Lora模块）即可完成通信.



[![r7neSJ.png](https://s3.ax1x.com/2020/12/28/r7neSJ.png)](https://imgchr.com/i/r7neSJ)



## 项目工程

链接：https://pan.baidu.com/s/11a10D74DWsndnA8oul5PYg 
提取码：u3vh 